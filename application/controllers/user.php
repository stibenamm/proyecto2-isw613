<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
	}
	public function index()
	{
		if(($this->session->userdata('document_number')!=""))
		{
			$this->welcome();
		}
		else{
			$data['title']= 'login';
			$this->load->view("registration_view.php", $data);
		}
	}
	public function welcome()
	{
		$data['title']= 'Welcome';
		$this->load->view('dashboard.php', $data);
	}
	public function login()
	{
		$document_number=$this->input->post('document_number');
		$password=md5($this->input->post('pass'));

		$result=$this->user_model->login($document_number,$password);
		if($result) $this->welcome();
		else        $this->index();
	}

	public function registration()
	{
		$this->load->library('form_validation');
		// field name, error message, validation rules
		$this->form_validation->set_rules('document_number', 'Cedula', 'trim|required|min_length[4]|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
		$this->form_validation->set_rules('con_password', 'Password Confirmation', 'trim|required|matches[password]');

		if($this->form_validation->run() == FALSE)
		{
			$this->index();
		}
		else
		{
			$this->user_model->add_user();
			$this->index();
		}
	}
	public function logout()
	{
		$newdata = array(
		'id'   =>'',
		'document_number'  =>'',
		'logged_in' => FALSE,
		);
		$this->session->unset_userdata($newdata );
		$this->session->sess_destroy();
		$this->index();
	}
}
?>
