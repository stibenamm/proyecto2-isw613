<!DOCTYPE html>
<html>
	<head>	
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
		<link class="cssdeck" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.1/css/bootstrap-responsive.min.css" class="cssdeck">
		</head>
	<body>
	
	<div class="page-header">
	  <h1>UTN Proyecto</h1>
	</div>


	<div class="" id="loginModal">
		<div class="modal-body">
			<div class="well">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#login" data-toggle="tab">Ingresar</a></li>
					<li><a href="#create" data-toggle="tab">Registrarse</a></li>
				</ul>
				<div id="myTabContent" class="tab-content">
					<div class="tab-pane active in" id="login">
						<fieldset>
							<?php echo validation_errors('<p class="error">'); ?>
							<?php  $attributes = array('class' => 'form-signin');
							echo form_open("user/login"); ?>
									<input class="input-xlarge" type="text" id="document_number" name="document_number" value="" placeholder="Cedula"/> <br>  
									<input class="input-xlarge" type="password" id="pass" name="pass" value="" placeholder="Contraseña"/> <br>  
									<input type="submit" class="btn btn-primary btn-lg" value="Ingresar" />
							<?php echo form_close(); ?>
						</fieldset>              
					</div>

					<div class="tab-pane fade" id="create">
						<fieldset>
							<?php echo validation_errors('<p class="error">'); ?>
							<?php  $attributes = array('class' => 'form-signin');
							echo form_open("user/registration",$attributes); ?>
								<input class="input-xlarge" type="text" id="document_number" name="document_number" value="<?php echo set_value('document_number'); ?>" placeholder="Cedula" /><br> 
								<input class="input-xlarge" type="password" id="password" name="password" value="<?php echo set_value('password'); ?>" placeholder="Contraseña" /><br>
								<input class="input-xlarge" type="password" id="con_password" name="con_password" value="<?php echo set_value('con_password'); ?>" placeholder="Repita su contraseña"/><br>
								<input type="submit" class="btn btn-primary btn-lg" value="Registrar" />
							<?php echo form_close(); ?>
						</fieldset>		
					</div>	
				</div>
			</div>
		</div>

	<script class="cssdeck" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script class="cssdeck" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
	</body>
	</html>