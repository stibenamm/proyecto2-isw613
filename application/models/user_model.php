<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }
    function login($document_number,$password)
    {
        $this->db->where("document_number",$document_number);
        $this->db->where("password",$password);
            
        $query=$this->db->get("user");
        if($query->num_rows()>0)
        {
            foreach($query->result() as $rows)
            {
                //add all data to session
                $newdata = array(
                        'user_id'       => $rows->id,
                        'document_number'   => $rows->document_number,
                        'logged_in'     => TRUE,
                   );
            }
                $this->session->set_userdata($newdata);
                return true;            
        }
        return false;
    }
    public function add_user()
    {
        $data=array(
            'document_number'=>$this->input->post('document_number'),
            'password'=>md5($this->input->post('password')),
            'created_at' => date("Y-m-d H:i:s", time()),
            'updated_at' => date("Y-m-d H:i:s", time())
            );
        $this->db->insert('user',$data);
    }
}
?>