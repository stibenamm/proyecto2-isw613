<h1><?php echo $title; ?></h1>
		<?php echo $message; ?>
		<form method="post" action="<?php echo $action; ?>">
		<div class="data">
		<table>
			<tr>
				<td width="30%">ID</td>
				<td><input type="text" name="id" disabled="disable" class="text" value="<?php echo set_value('id'); ?>"/></td>
				<input type="hidden" name="id" value="<?php echo set_value('id',$this->form_data->id); ?>"/>
			</tr>
			<tr>
				<td valign="top">Curso<span style="color:red;">*</span></td>
					 <?php echo form_dropdown('course_id', $this->form_data->course_id);  ?>
				</td>
			</tr>
			<tr>
				<td valign="top">Quarter<span style="color:red;">*</span></td>
				<td><input type="text" name="quarter" class="text" value="<?php echo set_value('quarter',$this->form_data->quarter); ?>"/>
				<?php echo form_error('quarter'); ?>
				</td>
			</tr>
			<tr>
				<td valign="top">Profesor<span style="color:red;">*</span></td>
					 <?php echo form_dropdown('professor_id', $this->form_data->professor_id);  ?>
				</td>
			</tr>
			<tr>
				<td valign="top">N Grupo<span style="color:red;">*</span></td>
				<td><input type="text" name="group_number" class="text" value="<?php echo set_value('group_number',$this->form_data->group_number); ?>"/>
				<?php echo form_error('group_number'); ?>
				</td>
			</tr>
			<tr>
				<td valign="top">Disponible(0 o 1)<span style="color:red;">*</span></td>
				<td><input type="text" name="enabled" class="text" value="<?php echo set_value('enabled',$this->form_data->enabled); ?>"/>
				<?php echo form_error('enabled'); ?>
				</td>
			</tr>

			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" value="Guardar"/></td>
			</tr>
		</table>
		</div>
		</form>
		<br />
		<?php echo $link_back; ?>
