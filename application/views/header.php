<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Aula</title>

<link href="<?php echo base_url(); ?>res/css/style.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url(); ?>res/css/calendar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>res/js/calendar.js"></script>

</head>
<body>
  <div class="logo">
    <img src="<?php echo base_url(); ?>res/logo.png" />
  </div>
  <div class="nav">
    <ul>
    <li><a href="<?=site_url('/student');?>">Estudiantes</a></li>
    <li><a href="<?=site_url('/professor');?>">Profesores</a></li>
    <li><a href="<?=site_url('/classroom');?>">Aulas</a></li>
    <li><a href="<?=site_url('/course');?>">Cursos</a></li>
    <li><a href="<?=site_url('/group');?>">Grupos</a></li>
    <li><a href="<?=site_url('/user_maint');?>">Usuarios</a></li>
    </ul>
  </div>
  <div class="content">