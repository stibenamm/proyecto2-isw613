		<h1><?php echo $title; ?></h1>
		<?php echo $message; ?>
		<form method="post" action="<?php echo $action; ?>">
		<div class="data">
		<table>
			<tr>
				<td width="30%">ID</td>
				<td><input type="text" name="id" disabled="disable" class="text" value="<?php echo set_value('id'); ?>"/></td>
				<input type="hidden" name="id" value="<?php echo set_value('id',$this->form_data->id); ?>"/>
			</tr>
			<tr>
				<td valign="top">Nombre<span style="color:red;">*</span></td>
				<td><input type="text" name="first_name" class="text" value="<?php echo set_value('first_name',$this->form_data->first_name); ?>"/>
				<?php echo form_error('first_name'); ?>
				</td>
			</tr>
			<tr>
				<td valign="top">Apellidos<span style="color:red;">*</span></td>
				<td><input type="text" name="last_name" class="text" value="<?php echo set_value('last_name',$this->form_data->last_name); ?>"/>
				<?php echo form_error('last_name'); ?>
				</td>
			</tr>
			<tr>
				<td valign="top">Cedula<span style="color:red;">*</span></td>
				<td><input type="text" name="document_number" class="text" value="<?php echo set_value('document_number',$this->form_data->document_number); ?>"/>
				<?php echo form_error('document_number'); ?>
				</td>
			</tr>
			<tr>
				<td valign="top">Correo<span style="color:red;">*</span></td>
				<td><input type="text" name="email" class="text" value="<?php echo set_value('email',$this->form_data->email); ?>"/>
				<?php echo form_error('email'); ?>
				</td>
			</tr>

			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" value="Guardar"/></td>
			</tr>
		</table>
		</div>
		</form>
		<br />
		<?php echo $link_back; ?>